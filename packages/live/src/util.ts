import { LiveFiber, Task, Action, Dispatcher, Key } from './types';

const NO_DEPS = [] as any[];

const DEBUG = false;

// Schedules actions to be run immediately after the current thread completes
export const makeActionScheduler = () => {
  const queue = [] as Action<any>[];

  let task: unknown;
  let cancel: (() => void) | undefined;
  let onUpdate = null as any;

  const bind = (f: Dispatcher) => onUpdate = f;

  const scheduleTask = <T = unknown>(createTask: (fn: () => void) => T, fn: () => void, cancelTask?: (task: T) => void) => {

    cancel = scopedCancel;

    const currentTask = task = createTask(scopedTask);

    function scopedCancel() {
      cancelTask?.(currentTask);
      // If no new schedule has been posted, clear
      if (currentTask === task) {
        task = undefined;
        cancel = undefined;
      }
    }

    function scopedTask() {
      // If a new schedule has been posted, skip
      if (currentTask === task) {
        task = undefined;
        cancel = undefined;
        fn();
      }
    }
  }

  const scheduleTimeout = (fn: () => void) => {

    scheduleTask(
        (resolve) => {
          if (DEBUG) {
            console.log("scheduleTimeout");
          }
          return setTimeout(resolve, 0);
        },
        fn,
        (task) => {
          clearTimeout(task);
        }
    );

  };

  const scheduleMicrotask = (fn: () => void) => {

    scheduleTask(
        (resolve) => {
          if (DEBUG) {
            console.log("scheduleMicrotask");
          }
          queueMicrotask(resolve);
          return Symbol("Microtask");
        },
        fn
    );

  };

  const scheduleAnimationFrame = (fn: () => void) => {

    scheduleTask(
        (resolve) => {
          if (DEBUG) {
            console.log("scheduleAnimationFrame");
          }
          requestAnimationFrame(resolve);
          return Symbol("Animation Frame");
        },
        fn
    );

  };

  const schedule = (fiber: LiveFiber<any>, task: Task) => {
    queue.push({fiber, task});
    if (!cancel) {
      if (Math.random() > 0.5) {
        scheduleTimeout(flush);
      } else {
        if (typeof requestAnimationFrame && Math.random() > 0.5) {
          scheduleAnimationFrame(flush);
        } else {
          scheduleMicrotask(flush);
        }
      }
    }
  };

  const flush = () => {
    const q = queue.slice();
    queue.length = 0;
    cancel?.();
    cancel = undefined;

    for (const {task} of q) task();
    if (onUpdate) {
      onUpdate(q);
    }
  };

  return {bind, schedule, flush};
}

// Tracks long-range dependencies for contexts
export const makeDependencyTracker = () => {
  const dependencies = new WeakMap<LiveFiber<any>, Set<LiveFiber<any>>>();

  const depend = (fiber: LiveFiber<any>, root: LiveFiber<any>) => {
    let list = dependencies.get(root);
    if (!list) dependencies.set(root, list = new Set());

    let exist = list.has(fiber);
    if (!exist) list.add(fiber);
    return !exist;
  }

  const undepend = (fiber: LiveFiber<any>, root: LiveFiber<any>) => {
    let list = dependencies.get(root);
    if (list) list.delete(fiber);
  }

  const invalidate = (fiber: LiveFiber<any>) => {
    const fibers = dependencies.get(fiber);
    return fibers ? Array.from(fibers.values()) : NO_DEPS;
  }

  return {depend, undepend, invalidate};
}


// Schedules actions to be run when an object is disposed of
export const makeDisposalTracker = () => {
  const disposal = new WeakMap<LiveFiber<any>, Task[]>();

  const track = (fiber: LiveFiber<any>, t: Task) => {
    let list = disposal.get(fiber);
    if (!list) disposal.set(fiber, list = []);
    list.push(t);
  }

  const dispose = (fiber: LiveFiber<any>) => {
    const tasks = disposal.get(fiber);
    if (tasks) {
      disposal.delete(fiber);
      for (const task of tasks) task();
    }
  }

  return {track, dispose};
}

// Schedules callback(s) on next paint
export const makePaintRequester = (raf: any = requestAnimationFrame) => {
  let pending = false;
  const queue: Task[] = [];

  const flush = () => {
    const q = queue.slice();
    queue.length = 0;
    pending = false;

    for (const t of q) t();
  }

  return (t: Task) => {
    if (!pending) {
      pending = true;
      raf(flush);
    }
    queue.push(t);
  }
}

// Compares dependency arrays
export const isSameDependencies = (
  prev: any[] | undefined,
  next: any[] | undefined,
) => {
  let valid = true;
  if (next === undefined && prev === undefined) return true;
  if (prev === undefined) valid = false;
  if (next != null && prev != null) {
    const n = prev.length || 0;
    if (n !== next.length || 0) valid = false;
    else for (let i = 0; i < n; ++i) if (prev[i] !== next[i]) {
      valid = false;
      break;
    }
  }
  return valid;
}

// Checks if one path is a subpath of another
export const isSubPath = (a: Key[], b: Key[]) => {
  if (b.length <= a.length) return false;
  const n = a.length;
  for (let i = 0; i < n; ++i) if (a[i] !== b[i]) return false;
  return true;
}

// Checks if one path is a subpath of another
export const isSubOrSamePath = (a: Key[], b: Key[]) => {
  if (b.length < a.length) return false;
  const n = a.length;
  for (let i = 0; i < n; ++i) if (a[i] !== b[i]) return false;
  return true;
};

